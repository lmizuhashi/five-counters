function View(args) {}

View.prototype.getHtml = require("./getHtml/definition.js");
View.prototype.appendTo = require("./appendTo/definition.js");
View.prototype.bindEventHandlers = require("./bindEventHandlers/definition.js");

module.exports = View;

