var State = require("../../state/constructor.js");

function startPolling(args) {

  var classScope = this;

  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open("GET","/model");
  xmlHttp.onreadystatechange = handleReadyStateChange;
  classScope.polling = true;
  xmlHttp.send(null);

  function handleReadyStateChange() {
    if(xmlHttp.status === 200) {
      if (xmlHttp.readyState === XMLHttpRequest.DONE) {
        var result;
        try {
          result = JSON.parse(xmlHttp.response);
        } catch (error) {
          classScope.controller.handlePollingFailure({
            error: error 
          });
          classScope.stopPolling();
        }
        if (result) {
          classScope.state = result;
          classScope.controller.handlePollingSuccess(
            State.prototype.remoteToLocal(result)
          );
        }
        if (classScope.polling) {
          setTimeout(startPolling.bind(classScope),classScope.pollingDelay);
        }
      }
    } else {
      classScope.controller.handlePollingFailure({
        error: new Error("Polling of server state failed")
      });
    }
  }
}

module.exports = startPolling;

