var Handlebars = require("handlebars");
var uuid = require("uuid");
var Observable = require("../observable/constructor.js");
var View = require("../view/constructor.js");

function Arrow(args) {

  var classScope = this;

  classScope.sourceCounter = args.sourceCounter;
  classScope.targetCounter = args.targetCounter;

  classScope.viewModel = {
    left: (args.direction === "left")
  };

  classScope.eventBindings = [
    ["click",classScope.handleClick.bind(classScope)]
  ];

  args.styleController.registerStyle({
    style: classScope.style,
    classId: classScope.classId
  });
}

Arrow.prototype.template = require("./template.html").trim();
Arrow.prototype.renderer = Handlebars.compile(Arrow.prototype.template);
Arrow.prototype.style = require("./style.css").trim();
Arrow.prototype.classId = uuid();
Arrow.prototype.handleClick = require("./handleClick/definition.js");

var ThisClass = Arrow;
Object.keys(View.prototype).forEach(appendViewToClass);
function appendViewToClass(key) {
  ThisClass.prototype[key] = View.prototype[key];
}
Object.keys(Observable.prototype).forEach(appendToClass);
function appendToClass(key) {
  ThisClass.prototype[key] = Observable.prototype[key];
}

module.exports = Arrow;

