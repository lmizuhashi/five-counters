function handleClick(event) {

  var classScope = this;

  var args;

  classScope.controller.handleArrowClick({
    sourceCounter: classScope.sourceCounter.id,
    targetCounter: classScope.targetCounter.id
  });
}

module.exports = handleClick;

