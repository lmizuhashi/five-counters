from session import Session

class Database(object):

  def __init__(self):
    self.session = {}

  def __assertSessionExists(self,sessionId):
    if sessionId not in self.session:
      raise KeyError(sessionId + " does not exist in database")

  def __assertSessionNotExists(self,sessionId):
    if sessionId in self.session:
      raise KeyError(sessionId + " already exists in database")

  def createSession(self,sessionId,numberOfCounters):
    self.__assertSessionNotExists(sessionId)
    self.session[sessionId] = Session(numberOfCounters)

  def getSession(self,sessionId):
    self.__assertSessionExists(sessionId)
    self.session[sessionId].update()
    return self.session[sessionId]

  def endSession(self,sessionId):
    self.__assertSessionExists(sessionId)
    del self.session[sessionId]

  def resetCounter(self,sessionId,counterId):
    self.__assertSessionExists(sessionId)
    return self.session[sessionId].reset(counterId)

  def transferCounter(self,sessionId,sourceCounter,targetCounter):
    self.__assertSessionExists(sessionId)
    return self.session[sessionId].transfer(sourceCounter,targetCounter)
    
