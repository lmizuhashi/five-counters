var Handlebars = require("handlebars");
var uuid = require("uuid");
var Observable = require("../observable/constructor.js");
var View = require("../view/constructor.js");
var displayStrings = require("./displayStrings.json");

function Counter(args) {

  var classScope = this;

  classScope.id = args.DELTA; 
  classScope.viewModel = {
    counterId: args.DELTA,
    value: args.value,
    resetButtonLabel: displayStrings.reset[language] || displayStrings.reset["en-US"]
  };

  classScope.eventBindings = [
    ["click",classScope.handleResetClick.bind(classScope)]
  ];

  args.styleController.registerStyle({
    style: classScope.style,
    classId: classScope.classId
  });
}

Counter.prototype.template = require("./template.html").trim();
Counter.prototype.renderer = Handlebars.compile(Counter.prototype.template);
Counter.prototype.style = require("./style.css").trim();
Counter.prototype.classId = uuid();
Counter.prototype.update = require("./update/definition.js");
Counter.prototype.handleResetClick = require("./handleResetClick/definition.js");

var ThisClass = Counter;
Object.keys(View.prototype).forEach(appendViewToClass);
function appendViewToClass(key) {
  ThisClass.prototype[key] = View.prototype[key];
}
Object.keys(Observable.prototype).forEach(appendToClass);
function appendToClass(key) {
  ThisClass.prototype[key] = Observable.prototype[key];
}

module.exports = Counter;

