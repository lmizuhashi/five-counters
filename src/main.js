var AppController = require("./appController/constructor.js");
var Database = require("./database/constructor.js");
var StyleController = require("./styleController/constructor.js");
var Stage = require("./stage/constructor.js");
var Counter = require("./counter/constructor.js");
var Arrow = require("./arrow/constructor.js");

var database = new Database({
  state: state
});
var styleController = new StyleController();
var stage = new Stage({
  styleController: styleController
});

var counters = [];
var arrows = [];
state.counter.forEach(instantiateCounters);
function instantiateCounters(counterModel,index) {
  counters.push(
    new Counter({
      DELTA: counterModel.DELTA,
      value: counterModel.value,
      styleController: styleController
    })
  );
}
if (counters.length > 1) {
  arrows.push(
    new Arrow({
      sourceCounter: counters[0],
      targetCounter: counters[counters.length-1],
      direction: "left",
      styleController: styleController
    })
  );
  counters.forEach(instantiateArrows);
  arrows.push(
    new Arrow({
      sourceCounter: counters[counters.length-1],
      targetCounter: counters[0],
      direction: "right",
      styleController: styleController
    })
  );
}
function instantiateArrows(thisCounter,index,counters) {
  if (index < counters.length-1) {
    arrows.push(
      new Arrow({
        sourceCounter: counters[index+1],
        targetCounter: counters[index],
        direction: "left",
        styleController: styleController
      })
    );
    arrows.push(
      new Arrow({
        sourceCounter: counters[index],
        targetCounter: counters[index+1],
        direction: "right",
        styleController: styleController
      })
    );
  }
}

var appController = new AppController({
  database: database,
  stage: stage,
  styleController: styleController,  
  counters: counters,
  arrows: arrows
});

