var uuid = require("uuid");
var Observable = require("../observable/constructor.js");
var View = require("../view/constructor.js");

function Stage(args) {

  var classScope = this;

  args.styleController.registerStyle({
    style: classScope.style,
    classId: classScope.classId
  });
}

Stage.prototype.template = require("./template.html").trim();
Stage.prototype.style = require("./style.css").trim();
Stage.prototype.classId = uuid();

var ThisClass = Stage;
Object.keys(View.prototype).forEach(appendViewToClass);
function appendViewToClass(key) {
  ThisClass.prototype[key] = View.prototype[key];
}
Object.keys(Observable.prototype).forEach(appendToClass);
function appendToClass(key) {
  ThisClass.prototype[key] = Observable.prototype[key];
}

module.exports = Stage;

