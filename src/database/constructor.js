var Observable = require("../observable/constructor.js");

function Database(args) {

  var classScope = this;

  classScope.state = args.state;
  classScope.polling = false;
  classScope.pollingDelay = 500;
}

var ThisClass = Database;
Object.keys(Observable.prototype).forEach(appendToClass);
function appendToClass(key) {
  ThisClass.prototype[key] = Observable.prototype[key];
}

Database.prototype.startPolling = require("./startPolling/definition.js");
Database.prototype.stopPolling = require("./stopPolling/definition.js");
Database.prototype.reset = require("./reset/definition.js");
Database.prototype.transfer = require("./transfer/definition.js");

module.exports = Database;

