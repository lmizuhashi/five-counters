function State(args) {

  var classScope = this;
}

State.prototype.remoteToLocal = require("./remoteToLocal/definition.js");
State.prototype.localToRemote = require("./localToRemote/definition.js");

module.exports = State;

