function handleDOMContentLoaded(event) { 
  
  var classScope = this;

  classScope.stage.appendTo({
    node: document.body
  });
  var arrowIndex = 0;
  if (classScope.arrows.length) {
    classScope.arrows[arrowIndex].appendTo({
      node: classScope.stage.rootNode
    });
    arrowIndex++;
  }
  classScope.counters.forEach(renderCounters);
  function renderCounters(counter,index,counters) {
    counter.appendTo({
      node: classScope.stage.rootNode
    });
    if (classScope.arrows.length) {
      classScope.arrows[arrowIndex].appendTo({
        node: classScope.stage.rootNode
      });
      arrowIndex++;
      if (index !== classScope.counters.length-1) {
        classScope.arrows[arrowIndex].appendTo({
          node: classScope.stage.rootNode
        });
        arrowIndex++;
      }
    }
  }
  classScope.database.startPolling();
}

module.exports = handleDOMContentLoaded;

