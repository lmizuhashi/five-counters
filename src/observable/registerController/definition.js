function registerController(args) {

  var classScope = this;

  classScope.controller = args.controller;
}

module.exports = registerController;

