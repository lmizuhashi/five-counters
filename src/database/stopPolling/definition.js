function stopPolling(args) {

  var classScope = this;

  classScope.polling = false;
}

module.exports = stopPolling;

