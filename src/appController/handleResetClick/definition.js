function handleResetClick(args) {

  var classScope = this;

  classScope.database.reset({
    counterId: args.counterId,
    onSuccess: handleResetSuccess,
    onFailure: handleResetFailure
  });

  function handleResetSuccess(successArgs) {
    classScope.counters[args.counterId-1].update({
      value: successArgs.value
    });
  }

  function handleResetFailure(failureArgs) {

    console.error(failureArgs.error);
  }
}

module.exports = handleResetClick;

