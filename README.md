## Requirements

This project requires the following be installed in the runtime environment:

1. Flask
2. Node
3. NPM

## Installation

To install dependencies and start the app server:

1. Clone this repo to a Linux / Mac OS X machine `git clone https://gitlab.com/lmizuhashi/five-counters.git`
2. Navigate to the root directory of the clone created in step one `cd five-counters/`
3. Type `npm install`
    
## View the App

To view the application, go to `http://localhost:5000/` in a browser running on the same host as the app server.

The server maintains sessions, so closing and reopening your browser won't restart the counters. To restart the counters, restart the app server.

## Stop the App Server

To stop the app server:

1. Navigate to any directory in this repo
2. Type `npm run stopServer`

## Start the App Server

To restart the app server:

1. Navigate to any directory in this repo
2. Type `npm run startServer`

## View Server Logs

To view the app server's logs:

1. Navigate to any directory in this repo
2. Type `npm run viewServerLogs`

## Rebuild the Client

To rebuild the client for this project:

1. Navigate to any directory in this repo
2. Type `npm run buildClient`

## Ask Questions

To ask Luke questions about this repo, drop him a line via email or call him on his phone.

