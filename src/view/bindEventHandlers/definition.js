function bindEventHandlers(args) {

  var classScope = this;

  if (Array.isArray(classScope.eventBindings)) {
    classScope.eventBindings.forEach(iterateEventBindings);
    function iterateEventBindings(settingsArray) {
      var eventType = settingsArray[0];
      var handler = settingsArray[1];
      classScope.rootNode.addEventListener(eventType,handler);
    }
  }
}

module.exports = bindEventHandlers;

