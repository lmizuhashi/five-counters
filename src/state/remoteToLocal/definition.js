function remoteToLocal(state) {

  var local = { counter: [] };
  state.counter.forEach(iterateCounters);
  function iterateCounters(thisCounter) {
    local.counter.push({
      DELTA: thisCounter.DELTA,
      value: thisCounter.value
    });
  }
  return local;
}

module.exports = remoteToLocal;

