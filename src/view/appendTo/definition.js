function appendTo(args) {

  var classScope = this;

  var nodes = classScope.getHtml();
  args.node.appendChild(nodes);
  classScope.parent = args.node;
  classScope.rootNode = nodes; 
  classScope.bindEventHandlers();
}

module.exports = appendTo;

