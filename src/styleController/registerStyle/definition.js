function registerStyle(args) {

  var classScope = this;

  var thisStyleSheetIndex = classScope.getStyle({
    classId: args.classId
  });
  if (thisStyleSheetIndex >= 0) {
    document.styleSheets[thisStyleSheetIndex].count++;
  } else {
    var styleNode = document.createElement("style");
    var styleSelector = args.style.split("{")[0].trim();
    styleNode.appendChild(
      document.createTextNode(args.style)
    );
    classScope.head.appendChild(styleNode);

    var thisStyleSheet = document.styleSheets[document.styleSheets.length - 1];
    thisStyleSheet.classId = args.classId;
    thisStyleSheet.count = 1;
  }
}

module.exports = registerStyle;

