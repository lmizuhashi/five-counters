function AppController(args) {

  var classScope = this;

  classScope.database = args.database;
  classScope.stage = args.stage;
  classScope.styleController = args.styleController;  
  classScope.counters = args.counters;
  classScope.arrows = args.arrows;

  classScope.database.registerController({ controller: classScope });
  classScope.styleController.registerController({ controller: classScope });
  classScope.stage.registerController({ controller: classScope });
  classScope.counters.forEach(registerController);
  classScope.arrows.forEach(registerController);
  function registerController(thisInstance) {
    thisInstance.registerController({ controller: classScope });
  }

  document.addEventListener(
    "DOMContentLoaded",
    classScope.handleDOMContentLoaded.bind(classScope)
  );
}

AppController.prototype.handleDOMContentLoaded = require("./handleDOMContentLoaded/definition.js");
AppController.prototype.handlePollingSuccess = require("./handlePollingSuccess/definition.js");
AppController.prototype.handlePollingFailure = require("./handlePollingFailure/definition.js");
AppController.prototype.updateCounterView = require("./updateCounterView/definition.js");
AppController.prototype.handleResetClick = require("./handleResetClick/definition.js");
AppController.prototype.handleArrowClick = require("./handleArrowClick/definition.js");

module.exports = AppController;

