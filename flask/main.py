import jsonpickle
from uuid import uuid4
from os import urandom
from flask import Flask, session as flaskSession, render_template, send_from_directory, send_file, request, abort
from database import Database
from counter import Counter

SESSION_ID = "sessionId"
NUMBER_OF_COUNTERS = 5
database = Database()
application = Flask(__name__)
application.secret_key = urandom(24)

@application.route("/")
def index():
  if SESSION_ID not in flaskSession:
    flaskSession[SESSION_ID] = uuid4()

  if flaskSession[SESSION_ID] not in database.session:
    database.createSession(flaskSession[SESSION_ID],NUMBER_OF_COUNTERS)

  return render_template(
    "index.html",
    state=jsonpickle.encode(database.getSession(flaskSession[SESSION_ID])),
    language=request.headers["Accept-Language"].split(",")[0]
  )

@application.route("/model",methods=["GET"])
def getModel():
  if SESSION_ID not in flaskSession:
    abort(401) 
  else:
    return jsonpickle.encode(database.getSession(flaskSession[SESSION_ID]))

@application.route("/model/<int:counterId>",methods=["POST"])
def resetCounter(counterId):
  if SESSION_ID not in flaskSession:
    abort(401) 
  else:
    return jsonpickle.encode(
      database.resetCounter(flaskSession[SESSION_ID],counterId)
    )

@application.route("/model/<int:sourceCounter>/<int:targetCounter>",methods=["POST"])
def transferCounter(sourceCounter,targetCounter):
  if SESSION_ID not in flaskSession:
    abort(401) 
  else:
    return jsonpickle.encode(
      database.transferCounter(flaskSession[SESSION_ID],sourceCounter,targetCounter)
    )

@application.route("/main.js")
def getClient():
  return send_file("js/main.js")

@application.route("/css/<path:path>")
def send_css(path):
  return send_from_directory("css", path)

@application.route("/fonts/<path:path>")
def send_fonts(path):
  return send_from_directory("fonts", path)

if __name__ == "__main__":
  application.run(host="0.0.0.0")

