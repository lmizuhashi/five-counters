function update(args) {

  var classScope = this;

  classScope.viewModel.value = args.value;

  document
    .getElementById(classScope.id)
    .getElementsByTagName("value")[0].innerHTML = args.value; 
}

module.exports = update;

