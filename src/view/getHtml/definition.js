function getHtml(args) {

  var classScope = this;

  var htmlString;
  if (typeof classScope.renderer === "function") {
    htmlString = classScope.renderer(classScope.viewModel);
  } else {
    htmlString = classScope.template;
  }

  var nodes = document.createElement('div');
  nodes.innerHTML = htmlString;
  return nodes.firstChild;
}

module.exports = getHtml;

