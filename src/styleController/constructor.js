var Observable = require("../observable/constructor.js");

function StyleController(args) {

  var classScope = this;
}

StyleController.prototype.getStyle = require("./getStyle/definition.js");
StyleController.prototype.registerStyle = require("./registerStyle/definition.js");
StyleController.prototype.head = document.head;

var ThisClass = StyleController;
Object.keys(Observable.prototype).forEach(appendToClass);
function appendToClass(key) {
  ThisClass.prototype[key] = Observable.prototype[key];
}

module.exports = StyleController;

