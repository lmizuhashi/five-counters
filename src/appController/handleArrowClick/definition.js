function handleArrowClick(args) {

  var classScope = this;

  classScope.database.transfer({
    sourceCounter: args.sourceCounter,
    targetCounter: args.targetCounter,
    onSuccess: handleTransferSuccess,
    onFailure: handleTransferFailure
  });

  function handleTransferSuccess(successArgs) {
    classScope.counters[args.targetCounter-1].update({
      value: successArgs.value
    });
  }

  function handleTransferFailure(failureArgs) {

    console.error(failureArgs.error);
  }
}

module.exports = handleArrowClick;

