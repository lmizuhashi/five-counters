function handlePollingSuccess(args) {

  var classScope = this;

  classScope.updateCounterView(args);
}

module.exports = handlePollingSuccess;

