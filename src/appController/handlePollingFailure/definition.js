function handlePollingFailure(args) {

  var classScope = this;
  throw args.error;
}

module.exports = handlePollingFailure;

