function handleResetClick(event) {

  var classScope = this;

  classScope.controller.handleResetClick({
    counterId: classScope.id
  });
}

module.exports = handleResetClick;

