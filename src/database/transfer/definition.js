function transfer(args) {

  var classScope = this;

  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open("POST","/model/" + args.sourceCounter + "/" + args.targetCounter);
  xmlHttp.onreadystatechange = handleReadyStateChange;
  xmlHttp.send(null);

  function handleReadyStateChange() {
    if(xmlHttp.status === 200) {
      if (xmlHttp.readyState === XMLHttpRequest.DONE) {
        var result;
        try {
          result = JSON.parse(xmlHttp.response);
        } catch (error) {
          args.onFailure({
            error: error 
          });
        }
        if (result) {
          classScope.state = result;
          args.onSuccess(result);
        }
      }
    } else {
      args.onFailure({
        error: new Error(
          "Transfer from counter " + args.sourceCounter + " to "
          + args.targetCounter + " failed"
        )
      });
    }
  }
}

module.exports = transfer;

