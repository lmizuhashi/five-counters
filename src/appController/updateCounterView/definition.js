function updateCounterView(args) {

  var classScope = this;

  var foundCounter;
  var counterView;
  args.counter.forEach(iterateNewData);
  function iterateNewData(thisCounterModel) {
    foundCounter = false;
    for (var i = 0; i < classScope.counters.length; i++) {
      foundCounter = (classScope.counters[i].id === thisCounterModel.DELTA);
      if ( foundCounter ) {
        counterView = classScope.counters[i];
        break;
      }
    }
    if (!foundCounter) {
      throw new Error("Can't find counter " + thisCounterModel.DELTA);
    }
    counterView.update(thisCounterModel); 
  }
}

module.exports = updateCounterView;

