from time import time

class Counter(object):

  def __init__(self,delta):
    self.lastUpdated = self.__now() 
    self.DELTA = delta 
    self.value = 0

  def __now(self):
    return int(round(time()))

  def update(self):
    now = self.__now() 
    self.value += (now - self.lastUpdated) * self.DELTA
    self.lastUpdated = now

  def reset(self):
    self.value = 0

  def getValue(self):
    return self.value

  def setValue(self,newValue):
    self.value = newValue
