function getStyle(args) {

  var classScope = this;

  var thisStyleSheetIndex = -1;
  for (var i = 0; i < document.styleSheets.length; i++) {
    if (document.styleSheets[i].classId === args.classId) {
      thisStyleSheetIndex = i;
      break;
    }
  }
  return thisStyleSheetIndex;
}

module.exports = getStyle;

