from counter import Counter

class Session(object):

  def __init__(self,numberOfCounters):
    self.counter = []
    for i in range(numberOfCounters):
      self.counter.append(Counter(i+1))
    
  def update(self):
    for i in range(len(self.counter)):
      self.counter[i].update()

  def reset(self,counterId):
    if counterId-1 >= len(self.counter):
      raise KeyError(str(counterId) + " not in session counter list");
    self.counter[counterId-1].reset()
    return self.counter[counterId-1]

  def transfer(self,sourceCounter,targetCounter):
    if sourceCounter-1 >= len(self.counter):
      raise KeyError(str(sourceCounter) + " not in session counter list");
    if targetCounter-1 >= len(self.counter):
      raise KeyError(str(targetCounter) + " not in session counter list");
    self.counter[targetCounter-1].setValue(
      self.counter[sourceCounter-1].getValue()
    )
    return self.counter[targetCounter-1]

